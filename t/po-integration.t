#!/usr/bin/perl
use warnings;
use strict;

use Cwd;
use Test::More;
use File::Basename;
use File::Spec;
use File::Temp qw{tempdir};

use Test::More;

BEGIN { use_ok("IkiWiki"); }

my $installed = $ENV{INSTALLED_TESTS};

my @command;
if ($installed) {
	ok(1, "running installed");
	@command = qw(ikiwiki);
}
else {
	ok(! system("make -s ikiwiki.out"));
	@command = ("perl", "-I".getcwd, qw(./ikiwiki.out
		--underlaydir=underlays/basewiki
		--set underlaydirbase=underlays
		--templatedir=templates));
}

my $dir = tempdir("ikiwiki-test-po.XXXXXXXXXX",
		  DIR => File::Spec->tmpdir,
		  CLEANUP => 1);

push @command, (qw(--set po_master_language=en|English
	--set po_translatable_pages=index
	--set-yaml po_slave_languages=[es|Spanish,de|German]
	--set-yaml po_disabled_languages=[de]
	--plugin po
	--verbose),
	"$dir/in", "$dir/out");

ok(! system("mkdir -p $dir/in"));

sub update_source_and_validate_refresh {
	my $source_text = shift;

	writefile("in/index.mdwn", $dir, $source_text);

	# We have to wait 1 second here so that new writes are guaranteed
	# to have a strictly larger mtime.
	sleep 1;

	ok(! system(@command), 'run ikiwiki command');

	foreach my $ext (qw{pot de.po es.po}) {
		my $file = "${dir}/in/index.$ext";
		ok(-e $file, "$file exists");
		like(readfile($file), qr/^msgid "$source_text"/m, "$file is up-to-date");
	}

	ok(-e "${dir}/out/index.en.html",   "English was rendered");
	ok(-e "${dir}/out/index.es.html",   "Spanish was rendered");
	ok(! -e "${dir}/out/index.de.html", "German was not rendered");

}

update_source_and_validate_refresh("This is a test line");
update_source_and_validate_refresh("This is another test line");

# FIXME: these links are only updated after the second ikiwiki run:
# the first one generates the PO files correctly but the resulting
# HTML files lack links to the same page in other languages.

my $content = readfile("${dir}/out/index.en.html");
like($content, qr/index[.]es[.]html/, 'link to Spanish');
unlike($content, qr/index[.]de[.]html/, 'no link to German');

$content = readfile("${dir}/out/index.es.html");
like($content, qr/index[.]en[.]html/, 'link to English');
unlike($content, qr/index[.]de[.]html/, 'no link to German');

done_testing;
